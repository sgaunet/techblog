# TechBlog

This is my tech blog to expose some source code I'm writing when I'm not at work. Yes, I'm a ansible/docker/k8s enthusiast.

[The site is here !](https://tech.krusaf.org)

## Extraction

```
$ git clone ...
$ git submodule init
Sous-module 'themes/docdock' (https://github.com/vjeantet/hugo-theme-docdock.git) enregistré pour le chemin 'themes/docdock'
$ git submodule update
Clonage dans '/var/www/DOC-EXPL/themes/docdock'...
Chemin de sous-module 'themes/docdock' : '1d12f5733354d9bd4e19e439f068bdc3cfdabe4f' extrait
$ hugo
```


# Extra informations

## Install hugo

[Official site](https://gohugo.io/)]

## Init

```
hugo new site . --force
git submodule add https://github.com/mazgi/hugo-theme-techlog-simple.git themes/techlog-simple
# Configure config.toml
```
