---
title: "Linux Desktop configuration"
description: "A set of ansible role to configure a Linux account"
Date: 2020-10-20
lastmod: 2020-10-20
tags: ["linux"]
categories: ["Dev"]
authors: ["Sylvain Gaunet"]
draft: false
---

I've just make a public repository where I have written some roles to install/configure a Linux desktop account. 
The prerequisite is ansible and to have an access to the root account for some setup.

<!--more-->

That's far away from perfection but that's a start and I will try to update it to be indepotent (actually, it's not).

[Here is the link to the repository.](https://github.com/sgaunet/conf-linux)


## Execution 

```
git clone git@github.com:sgaunet/conf-linux.git
sudo apt install -y ansible
cd conf-linux/src
<edit> installation.yml
./go.sh
```

## Roles

###  common

Install : cifs-utils','tmux','firefox','git','git-crypt','gpg','docker','jq'

###  awscli

Install the awscli to play with Amazon Web Service API.

###  packer

Install packer.

###  typora

Install Typora.

###  vagrant

Install vagrant.

###  vscode

Install vscode from official repository.

###  zsh

Install zsh, ohmyzsh and configure the ~/.zshrc with agnoster theme. 

### kubectl

Install kubectl command

### kubectx-ns

Install :

* kubens
* kubectx


### helm

Install helm v3

### kube-ps1

To get a beautiful prompt (for bash or zsh)

![prompt](/conf-linux/prompt.png)
      