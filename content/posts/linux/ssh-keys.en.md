---
title: "Convert openssh keys to rsa keys"
date: 2023-01-22
# weight: 1
# aliases: ["/first"]
tags: ["linux","ssh"]
categories: ["ops"]
authors: ["Sylvain Gaunet"]
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
description: "Convert openssh keys to rsa keys"
# canonicalURL: "https://canonical.url/to/page"
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
# cover:
#     image: "<image path/url>" # image path/url
#     alt: "<alt text>" # alt text
#     caption: "<text>" # display caption under cover
#     relative: false # when using page bundles set this to true
#     hidden: true # only hide on current single page
# editPost:
#     URL: "https://github.com/<path_to_repo>/content"
#     Text: "Suggest Changes" # edit text
#     appendFilePath: true # to append file path to Edit link
---

# Convert openssh keys to rsa keys

from something that starts with

```
-----BEGIN OPENSSH PRIVATE KEY-----
```

to something that starts with

```
-----BEGIN RSA PRIVATE KEY-----
```

<!--more-->


## Prerequisites

You need puttygen.

```
sudo apt install putty-tools
```

## Procedure

```
# convert from openssh to ssh2 encrypted format
puttygen originalKeyFile -O private-sshcom -o /tmp/tmp.putty
# convert from ssh2 encrypted format to rsa
ssh-keygen -i -f /tmp/tmp.putty > rsaKeyFile
rm /tmp/tmp.putty
```

Voilà

