---
title: "Swarm vagrant"
description: "Create a swarm cluster with vagrant"
Date: 2020-06-01
lastmod: 2020-06-01
tags: ["docker","swarm", "vagrant"]
categories: ["Docker"]
authors: ["Sylvain Gaunet"]
cover:
    image: "vagrant-swarm.webp" # image path/url
    alt: "vagrant swarm" # alt text
    caption: "vagrant swarm" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: false # only hide on current single page
---

As for Kubernetes, I have done some code to build a swarm cluster with vagrant too :
<!--more-->

[The code is here](https://github.com/sgaunet/swarm-vagrant)

Follow the README to create the cluster. 

PS: I don't whereI found the code for the swarm which helped me a lot. If I find it again, I will add the link.