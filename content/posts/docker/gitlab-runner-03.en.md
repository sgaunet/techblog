---
title: "gitlab-runner in a swarm"
description: "Launch a gitlab-runner in a Swarm"
Date: 2020-07-04
lastmod: 2020-07-04
tags: ["docker","swarm", "gitlab"]
categories: ["Docker"]
authors: ["Sylvain Gaunet"]
---


We are beginning to use Swarm at work and I wanted to make a complete CI/CD in the Swarm. So I have tried to run my own gitlab-runner in the Swarm (connected to https://gitlab.com).

<!--more-->

## Register gitlab-runner

Before registering the gitlab-runner, find the token in gitlab :

* project
* settings -> CI/CD
* Runners -> expand

![CICD](/techblog/posts/cicd-token.png)

```
$ mkdir conf
$ docker run -ti -v $PWD/conf:/etc/gitlab-runner gitlab/gitlab-runner:latest register
Runtime platform                                    arch=amd64 os=linux pid=6 revision=21cb397c version=13.0.1
Running in system-mode.                            
                                                   
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.com
Please enter the gitlab-ci token for this runner:
**Indiquer le token récupéré dans le projet gitlab**
Please enter the gitlab-ci description for this runner:
[xxxXXXxxxXX]: Swarm Test
Please enter the gitlab-ci tags for this runner (comma separated):
test
Registering runner... succeeded                     runner=xxxZEERTVDFdf
Please enter the executor: docker+machine, docker-ssh+machine, ssh, virtualbox, docker-ssh, parallels, shell, kubernetes, custom, docker:
docker
Please enter the default Docker image (e.g. ruby:2.6):
dind:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded! 
$ ll conf
total 4,0K
-rw------- 1 root root 519 juin  18 21:11 config.toml
```

Fine, we have generated a configuration but we also have a client token associated to the server token.

```
$ sudo cat conf/config.toml
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Swarm Test"
  url = "https://gitlab.com"
  token = "token-client-gitlab-runner"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
  [runners.docker]
    tls_verify = false
    image = "dind:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
```

Update the volumes parameter to be able to to docker in docker.

```
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
```

The configuration should look like :

```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Swarm Test"
  url = "https://gitlab.com"
  token = "token-client-gitlab-runner"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
  [runners.docker]
    tls_verify = false
    image = "dind:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
```

For the Swarm, create a configuration :

```
docker config create gitlab conf/config.toml
```

Now, the docker-compose.yml :

```
version: '3.7'
services:
    gitlab:
        image: gitlab/gitlab-runner:alpine
        environment:
          - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
        command: run -n --tag-list docker,test --run-untagged=true --locked=false --executor docker 
        volumes: 
            - /var/run/docker.sock:/var/run/docker.sock 
        configs:
            - source: gitlab
              target: /etc/gitlab-runner/config.toml
        deploy:
            placement:
                constraints:
                - "node.role==worker"
            mode: replicated
            replicas: 1
            restart_policy:
               condition: any
            update_config:
               parallelism: 1
 
configs:
    gitlab:
        external: true
```

Launch it, you shoud see it as connected and available in your gitlab interface. 

```
docker stack deploy --prune --compose-file docker-compose-test.yml gitlab-runner
```

## Activate the gitlab-runner for the project

* Go the the project in gitlab
* Settings -> CI / CD
* Click on Runners
* Activate the swarm runner, desactivate the shared runners

![Activate the swarm runner, desactivate the shared runners](/techblog/posts/cicd-config-CI.png)

## Activate the runner 

Activate the runner 

![Activate the runner ](/techblog/posts/cicd-runners.png)

If needed, update the parameters of the runner

![If needed, update the parameters of the runner](/techblog/posts/cicd-conf-runner.png)

## Tags

Becareful with the tags associated to the runner, the jobs needs to have at least one tag of the associated runner's tags.In this example, the runner have the tags "test" and "docker". And my jobs have the tag "test".

Example of .gitlab-ci.yml :

```
stages:
    - build
    - deploy
    
build:
    stage: build
    only:
      - master
    image: docker:stable
    services:
      - docker:dind
    script:
      - docker login -u $REGISTRY_USER -p $REGISTRY_PASSWORD quay.io
      - docker build --build-arg VERSION=0.54.0 -t quay.io/login/monimage:latest .
      - docker push quay.io/login/monimage:latest
    tags:
      - test

deploy:
  stage: deploy
  when: on_success
  only:
    - master
  image: docker:stable
  services:
    - docker:dind
  script:
    - export DOCKER_HOST=tcp://${IP_SWARM}:2375
    - docker stack deploy --prune --compose-file docker-compose-test.yml MyStack
  tags:
    - test
```