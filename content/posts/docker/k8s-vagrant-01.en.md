---
title: "K8S vagrant"
description: "Create a k8s cluster with vagrant"
Date: 2020-03-04
lastmod: 2021-03-07
tags: ["k8s", "vagrant"]
categories: ["k8s"]
authors: ["Sylvain Gaunet"]
cover:
    image: "vagrant-ansible-k8s.webp" # image path/url
    alt: "vagrant k8s ansible" # alt text
    caption: "vagrant k8s ansible" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: false # only hide on current single page
---

Hello, I'beginning to learn Kubernetes and I have used vagrant and ansible to create a complete kubernetes cluster.

<!--more-->

[The code is here](https://github.com/sgaunet/k8s-vagrant)

Follow the README to create the cluster. 

In tutorials folder, there is some examples, some commands but actually, no comment. That's just memo for me. Maybe I will complete this later... don't know (depending time consuming)


Updated 2021-03-07 :  Sources have been updated to create a cluster with rke too. There is a little tutorial to create a rook/ceph cluster but I'm not sure that I will have the time to update this tutorial. I'm not an expert at all about ceph and the setup ask some bandwith that I don't have ... Too long to test ...