---
title: "GVM: Golang Manager"
description: "How to manage versions of GO"
Date: 2020-07-25
lastmod: 2020-07-25
tags: ["golang","dev"]
categories: ["Dev"]
authors: ["Sylvain Gaunet"]
draft: false
---

[GVM provides an interface to manage Go versions.](https://github.com/moovweb/gvm)

<!--more-->

## Install

Follow the README :

```
bash < <(curl -s -S -L https://raw.githubusercontent.com/moovweb/gvm/master/binscripts/gvm-installer)
```

Replace by zsh if you use zsh.
You will need to restart the terminal.

## Install go 

Install go version 1.14.6 :

```
$ gvm install go1.14.6          
Downloading Go source...
Installing go1.14.6...
 * Compiling...
go1.14.6 successfully installed!
```

Set the new version of GO as default :

```
$ gvm use go1.14.6 --default
Now using version go1.14.6
```

Check :

```
$ go version
go version go1.14.6 linux/amd64
```

Very useful !
